from Crypto.PublicKey import RSA
from tools import *
from Crypto.Hash import HMAC
from Crypto.Hash import SHA256

def get_RSA_keys(name):
    key = RSA.generate(1024)

    private='RSA_private_key_'+name+'.pem'
    public='RSA_public_key_'+name+'.pem'
    with open(private, 'wb') as f1,open(public, 'wb') as f2:
        f1.write(key.export_key('PEM'))
        f2.write(key.public_key().export_key('PEM'))

def get_session_key(master_secret,random_client,random_server):
    key_label=b'KEY'
    msg=bitwise_or_bytes(random_client,random_server)
    msg=bitwise_or_bytes(msg,key_label)

    hmac=HMAC.new(master_secret,msg,digestmod=SHA256).digest()
    return hmac[0:16],hmac[16:]

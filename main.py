from info import *
from getkeys import *

# messages
# (ClientHello||ServerHello||
# Hash(ServerCertificate)||
# Hash(ClientCertificate)||
# CertificateVerify||ClientKeyExchange);

messages = []

if __name__ == '__main__':
    info = Info()
    client_hello = info.build_client_hello()
    server_hello = info.build_server_hello()

    with open('RSA_public_key_server.pem', 'r') as f:
        public_key_bin = f.read().encode()

    server_certificate = info.build_server_certificate(public_key_bin)
    client_certificate = info.build_client_certificate(public_key_bin)

    key_client = RSA.import_key(open('RSA_private_key_client.pem').read())
    certificate_verify = info.build_certificate_verify(key_client, client_hello, server_hello, server_certificate)

    key_server = RSA.import_key(open('RSA_public_key_server.pem').read())

    client_key_exchange = info.build_client_key_exchange(key_server)

    messages.append(client_hello)
    messages.append(server_hello)
    messages.append(server_certificate)
    messages.append(client_certificate)
    messages.append(certificate_verify)
    messages.append(client_key_exchange)

    server_finished = info.build_server_finished(messages, client_key_exchange[3:])
    client_finished=info.build_client_finished(messages, client_key_exchange[3:])

    error_message=info.build_error_message(0x01.to_bytes(1,'big'))




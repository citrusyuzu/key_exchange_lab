from info import *
from getkeys import get_session_key
import socket


ip_port = ('127.0.0.1', 9999)

sk = socket.socket()            # 创建套接字
sk.bind(ip_port)                # 绑定服务地址
sk.listen(5)                    # 监听连接请求

print('启动socket服务，等待客户端连接')
conn, address = sk.accept()     # 等待连接，此处自动阻塞
print('连接建立')

messages=[]
info=Info()

client_reply = conn.recv(1024)
client_hello=client_reply
print('收到client_hello')
messages.append(client_hello)

server_hello=info.build_server_hello()
conn.sendall(server_hello)
print('发出server_hello')
messages.append(server_hello)

with open('RSA_public_key_server.pem', 'r') as f:
    public_key = f.read().encode()
server_certificate = info.build_server_certificate(public_key)
conn.sendall(server_certificate)
print('发出server_certificate')
messages.append(server_certificate)

client_reply = conn.recv(1024)
client_certificate=client_reply
print('收到client_certificate')
messages.append(client_certificate)

client_reply = conn.recv(1024)
certificate_verify=client_reply
print('收到certificate_verify')
messages.append(certificate_verify)

client_reply = conn.recv(1024)
client_key_exchange=client_reply
print('收到client_key_exchange')
messages.append(client_key_exchange)

server_finished=info.build_server_finished(messages, client_key_exchange[3:])
conn.sendall(server_finished)
print('发出server_finished')

client_reply = conn.recv(1024)
client_finished=client_reply
print('收到client_finished')

Skey,Mkey=get_session_key(client_key_exchange[3:],client_hello[3:],server_hello[3:])

# print(Skey)
# print(Mkey)

# print('test')
# print('client_hello')
# print(client_hello)
# print('server_hello')
# print(server_hello)
# print('server_certificate')
# print(server_certificate)
# print('client_certificate')
# print(client_certificate)
# print('certificate_verify')
# print(certificate_verify)
# print('client_key_exchange')
# print(client_key_exchange)
# print('server_finished')
# print(server_finished)
# print('client_finished')
# print(client_finished)


conn.close()    # 关闭连接
from Crypto.PublicKey import RSA
from getkeys import get_session_key
from info import *
import socket

ip_port = ('127.0.0.1', 9999)
s = socket.socket()     # 创建套接字
s.connect(ip_port)      # 连接服务器

messages=[]
info=Info()

client_hello = info.build_client_hello()
s.sendall(client_hello)
print('发出client_hello')
messages.append(client_hello)

server_reply = s.recv(1024)
server_hello=server_reply
print('收到server_hello')
messages.append(server_hello)

server_reply = s.recv(1024)
server_certificate=server_reply
print('收到server_certificate')
messages.append(server_certificate)

# 保存服务器的公钥
with open('new_public_key_server.pem', 'wb') as f:
    f.write(server_certificate[4:])

with open('RSA_public_key_client.pem', 'r') as f:
    public_key = f.read().encode()
client_certificate = info.build_client_certificate(public_key)
s.sendall(client_certificate)
print('发出client_certificate')
messages.append(client_certificate)

key_client = RSA.import_key(open('RSA_private_key_client.pem').read())
certificate_verify = info.build_certificate_verify(key_client, client_hello, server_hello, server_certificate)
s.sendall(certificate_verify)
print('发出certificate_verify')
messages.append(certificate_verify)

key_server = RSA.import_key(open('new_public_key_server.pem').read())
client_key_exchange = info.build_client_key_exchange(key_server)
s.sendall(client_key_exchange)
print('发出client_key_exchange')
messages.append(client_key_exchange)

server_reply = s.recv(1024)
server_finished=server_reply
print('收到server_finished')

client_finished=info.build_client_finished(messages, client_key_exchange[3:])
s.sendall(client_finished)
print('发出client_finished')

Skey,Mkey=get_session_key(client_key_exchange[3:],client_hello[3:],server_hello[3:])

# print(Skey)
# print(Mkey)

# print('test')
# print('client_hello')
# print(client_hello)
# print('server_hello')
# print(server_hello)
# print('server_certificate')
# print(server_certificate)
# print('client_certificate')
# print(client_certificate)
# print('certificate_verify')
# print(certificate_verify)
# print('client_key_exchange')
# print(client_key_exchange)
# print('server_finished')
# print(server_finished)
# print('client_finished')
# print(client_finished)

s.close()       # 关闭连接
